#### Bikes-API

##### A tool that scrapes webpages to search for bikes that a user can't afford

##### It takes in couple of parameters from the user and then shows bikes above the calculated range

##### Instructions to install - 
###### 1. git clone repo
###### 2. npm install to install dependencies.

###### (Any recommendations should be sent to shaarangtanpure@outlook.com.au)